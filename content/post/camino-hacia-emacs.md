+++
title = "El camino hacia Emacs"
author = ["Mickie"]
date = 2019-04-04T23:32:00-06:00
lastmod = 2020-12-16T23:53:57-06:00
tags = ["emacs", "linux", "gnu"]
draft = false
+++

{{< figure src="/blog/EmacsIcon.png" >}}

Desde el 2010 que fue cuando me decidí pasar a la plataforma del pingüino y el ñu
me sumergía en los sitios especializados de toda esta enredadera, empezaba a buscar
alternativas a los programas privativos de siempre (ofimática, navegador, etc.), a
su vez empezaba a tener acercamientos con la programación. Fue de estos "roces" que
veía en las cajas de comentarios y foros, de manera insistente, de un colosal programa
para controlar casi/toda tu informática, `Emacs`.

Como no ocupaba tanta cosa, solo un editor de texto simple con pocas características
para configurar, me quedé con dos: `Gedit` y `Geany`. Pero el gusto no duró para siempre,
como mis exigencias informáticas crecían como la espuma de cerveza en época de
"la calor", lo que ahora ocupaba era: **simplificar y optimizar mi actividad diaria frente al computador**.
Siendo uno de los grandes detonantes el poder gestionar lo más que pudiera por medio del **texto plano**.

Para saber más sobre este genial paradigma, recomiendo las siguientes lecturas:

[Descubre o redescubre el texto plano. Vuelve a lo sencillo.](https://victorhckinthefreeworld.com/2018/04/12/descubre-o-redescubre-el-texto-plano-vuelve-a-lo-sencillo/)

[Ventajas del texto plano](https://notxor.nueva-actitud.org/blog/2019/04/07/ventajas-del-texto-plano/)

> Emacs es un gran sistema operativo que necesita un mejor editor de texto.
> --- Un fiel seguidor de la iglesia de emacs

Después de ver los [video tutoriales](https://goblinrefuge.com/mediagoblin/u/farliz/) de [farliz](https://github.com/farliz) comencé a utilizarlo esporádicamente.
Hasta que a inicios de Diciembre del año pasado (2018) tome al ñu por los cuernos y me lancé al galope.

![](https://upload.wikimedia.org/wikipedia/en/3/36/Emacs-linux-console.png)
Fuente: Wikimedia Commons

**Este juguete no es para usuarios noveles**, para iniciar el andar en este "editor"
se tiene que tener en cuenta lo siguiente.


## Sabes de que va la cosa {#sabes-de-que-va-la-cosa}

Si te adentras en este extraño planeta, es que ya has visitado el resto del sistema solar.

-   Mas o menos **comprendes la filosofía que hay detrás** de todo esto

([Movimiento del software libre](https://es.wikipedia.org/wiki/Movimiento%5Fdel%5Fsoftware%5Flibre) y la [Cultura libre](https://es.wikipedia.org/wiki/Cultura%5Flibre), etc.).

-   Quieres **controlar y simplificar toda tu informática**.
-   Ocupas un programa que cubra, con creces, tus mas **radicales exigencias**.


## Nociones básicas de programación {#nociones-básicas-de-programación}

**No, no es necesario ser un programador** o experto en la materia, de hecho puedes
personalizar variables comunes como el tema de colores, tipografía, configuraciones básicas, etc.
desde un menú gráfico. Pero siendo sincero, se siente extremadamente torpe y lento este modo de
personalizar y al final te quedas corto de todo el potencial que puede ofrecer este editor;
por lo que es **recomendable picar codigo** para exprimirlo al máximo.
Emacs está contruido en una variante (dialecto) del lenguaje **lisp**, llamado **Emacs Lisp**, o
simplemente **elisp**, y los programas, llamados paquetes, escritos para él, son implementados
escribiendo en el archivo de configuración **init.el** código en **elisp**.

El mismo _Richard Stallman_ nos cuenta de esta particularidad

> Lo interesante de Emacs era que no solo incluía un lenguaje de programación, sino que además
> los comandos de edición de los que disponía el usuario estaban escritos en ese mismo lenguaje,
> de forma que permitía cargar nuevas instrucciones mientras se estaba editando. Podía modificar
> los programas que estaba utilizando e, inmediatamente, editar utilizando los programas recién
> modificados. Es decir, teníamos un sistema que resultaba útil para tratar con texto además de
> programar, y que, sin embargo, permitía al usuario programar el editor mientras lo estaba usando.
> No sé si era el primer sistema de estas características, pero, desde luego, sí el primer editor
> con tales prestaciones.


## Teclado mata ratón {#teclado-mata-ratón}

Bueno, le instalo todo eso, y ¿Como lo uso?, pues por medio de combinaciones de teclas (comandos),
hay otra manera mas gráfica, por medio del menú, pero al igual que el caso anterior, se siente cierta
incomodidad y algo de lentitud, así que a **practicar tu mecanografía**.


## Personalización ilimitada {#personalización-ilimitada}

Puedes hacerlo un gestor de ficheros, estación de programación, lector pdf, reproductor de musica,
ofimática, lector de feeds... **tú pones el limite**. Mi recomendación: enfócate en "para que
lo quieres y como lo quieres". **No le instales cosas que nunca usarás**.

Debido a las funcionalidades de elisp, existe una gran variedad de modos de configuración de esta poderosa
herramienta, observa las distintas configuraciones de los usuarios en [github](https://github.com/search?utf8=%25E2%259C%2593&q=emacs.d) y [gitlab](https://gitlab.com/search?utf8=%25E2%259C%2593&search=emacs.d&group%5Fid=&project%5Fid=&repository%5Fref=).
Explora, investiga, prueba y sobre todo juega con tu cacharro :smile:

Así es como llegué al "viejo ñu de la sabana".
