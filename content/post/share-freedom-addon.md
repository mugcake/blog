+++
title = "Share freedom addon"
author = ["Mickie"]
date = 2019-04-02T11:57:00-06:00
lastmod = 2020-12-17T01:02:07-06:00
tags = ["softwarelibre", "fediverso"]
draft = false
+++

**--Aviso--** El presente post está muy desactualizado, mucho ha cambiado desde entonces en la extensión. Visita el siguiente enlace [proximamente].

Hace unos meses navegando por los blog que sigo, me topé en el blog de viktor con el siguiente articulo:

[Cómo añadir un botón en tu wordpress para compartir contenido en #Mastodon #diaspora #gnusocial o #Hubzilla](https://victorhckinthefreeworld.com/2018/04/06/como-anadir-un-boton-en-tu-wordpress-para-compartir-contenido-en-mastodon-diaspora-gnusocial-o-hubzilla/)

Nos muestra una aplicación web llamada [Share freedom](https://victorhck.gitlab.io/share%5Ffreedom/) ,de allí viene el nombre de esta extensión :wink:, desarrollada
 por [kim](http://mastodont.cat/@surtdelcercle) y adaptada por el mismo viktor para poder compartir las paginas que incluyen dicho botón a las distintas
instancias dentro del _fediverso_.
**Un gran atino** teniendo en cuenta que una de las mas grandes ventajas de las redes federadas, es a su vez uno de sus
 mayores lastres en términos prácticos; **la descentralización**. Es súper cómodo compartir en las redes "sociales"
de la NSA ya que solo ocupas ingresar una sola dirección web, mientras que los usuarios en las redes libres sabemos que
hay cantidad de nodos/instancias como numero de granos en un cultivo de maíz.

Yo soy una persona que usa mas las redes, incluida la del condenado pájaro azul, como portal de noticias, pasando la
mayor parte del tiempo en ellas sin postear, sino compartiendo otros post (boost en mastodon) y compartir sitios de mi interés.
Aquí es donde la aplicación, debido a sus características, no me permite compartir cualquier
pagina, por lo que me vi en la necesidad de desarrollar una extensión para `firefox` que solventara (en parte) estas desventajas;


## Share freedom, extensión para el zorrito {#share-freedom-extensión-para-el-zorrito}

[Sitio para la instalar la extensión](https://addons.mozilla.org/en-US/firefox/addon/share-freedom/)

[Sitio del código fuente](https://gitlab.com/mickie1/share-freedom-addon)

¿Por qué una extensión para el navegador? si la montaña no va a ti, tu ve a la montaña;
en vez de preocuparse por ver si cierto sitio tiene botones para compartir en el fediverso, tú
navegador ahora tiene uno para compartir lo que te plazca. Compatible para todos los navegadores
derivados del zorro, como `Gnu icecat`, `iceweasel` y `abrowser` de la distro trisquel GNU/Linux.

Al estar algo _tosco_ en esto de la programación (estoy aprendiendo y estudiando por mi cuenta) y
moverme con cierta dificultad en el campo del javascript, decidí no complicarme
la vida, y hacer un **fork** de la extensión [URL-sharer](https://github.com/shivarajnaidu/URL-sharer) de yuvaraj (aka shivaraj) con licencia GPL v3.

Se puede ejecutar la app con la combinación de teclas `Crtl-Shift-u` que abre un pequeño panel como se
muestra en la imagen de arriba, con iconos de algunas de las plataformas libres mas populares, que al pinchar sobre
uno de ellos automáticamente comparte la pestaña activa y te redirecciona a una aplicación web para poder compartir
en tu instancia.

-   Diaspora, Friendica y Socialhome: hacen uso de [ Share on diaspora\*](https://share.diasporafoundation.org/about.html).
-   Pump.io: usa [Granada](https://www.autistici.org/granada/index.php) para explotar todo el potencial que puede ofrecer esta fantástica red.
-   Mastodon: usa [Mastoshare](https://mastoshare.net/) con el inconveniente de que esta en japones, pero fácil de entender.
-   Prismo (Una especie de reddit/meneame pero con la tecnología detrás de mastodon): aun no está listo para uso extendido, por lo que solo redirige a la [instancia oficial](https://prismo.news/).
-   Gnusocial: por ahora solo comparte al nodo gnusocial.no
-   Hubzilla: por ahora solo comparte al nodo star.hubzilla.org

No soporta **pleroma** , por problemas con los _http request_ que continúan fallando en su red.

**Nota**: tanto mastoshare como share on diaspora\* no soportan enlaces que contengan títulos con #hashtags.

**Cosas por hacer**

Implementar una página dentro de la propia extensión para introducir directamente las instancias. Espero hacerlo en un futuro próximo,
mientras maduro y pulo mejor mis conocimientos en esta materia.
