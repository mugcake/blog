+++
title = "Lector rss en Emacs"
author = ["Mickie"]
date = 2019-04-22T19:34:00-05:00
lastmod = 2020-12-17T00:07:22-06:00
tags = ["emacs", "code"]
draft = false
+++

![](/blog/rss.png)
Imagen: @plantabaja@mastodon.social

**RSS** es una gran herramienta que nos permite seguir todo tipo de sitios
que se actualizan con frecuencia como blogs, noticias, incluso las redes
sociales libres soportan este formato **xml**. Para mas info del formato
RSS consulta el articulo de [Alberto Moshpirit](https://datamost.com/Moshpirit): [RSS: Personaliza tu información](https://flosspirit.wordpress.com/2016/12/08/rss-personaliza-tu-informacion/)

Al ser un estándar de uso extendido en la telaraña de las
comunicaciones me resulta imprescindible para mantenerme al día y
también me permite tener una completa administración de lo que
sigo. Para leer tus `feeds` se pueden usar una infinidad de programas,
tanto "en-linea/nube" como para cualquier plataforma, en las distros
del **GNU con linux** el mejor para mi es `liferea`, disponible en los
repos de las mas populares.


## Una alternativa para emacs {#una-alternativa-para-emacs}

Al ser atrapado por la orbita de emacs, quise implementar un lector de
rss en el editor, y no fue difícil encontrar alternativa a liferea:

[El lector de feeds -Elfeed-](https://github.com/skeeto/elfeed)

{{< figure src="https://nullprogram.com/img/elfeed/search.png" >}}

Después de instalar solo agrega los feeds dentro del archivo de
configuración.

```emacs-lisp
(setq elfeed-feeds
      '("http://planet.emacsen.org/atom.xml"
	"http://planet.emacs-es.org/rss20.xml"))
```

Y para agregar etiquetas es de esta manera.

```emacs-lisp
(setq elfeed-feeds
      '(("http://nullprogram.com/feed/" blog emacs)
	"http://www.50ply.com/atom.xml"  ; no autotagging
	("http://nedroid.com/feed/" webcomic)))
```

Ejecutamos con `C-x W` y para actualizar los feeds presionamos `G`. Al
presionar `enter` abre la entrada en un buffer y con `b` la abre en el
navegador.


## Extensiones {#extensiones}

Vemos que tiene funciones muy limitadas, incluso para un programa para
interfaz de linea de comandos. Podemos ampliarlas añadiendo dos
paquetes más; [elfeed-goodies](https://github.com/algernon/elfeed-goodies) y [elfeed-org](https://github.com/remyhonig/elfeed-org).

El primero lo que hace es mejorar la interfaz que tiene por defecto,
que al abrir una entrada dentro de emacs, crea una ventana dentro del
mismo buffer, así como cambios estéticos en el powerline.

{{< figure src="https://raw.githubusercontent.com/algernon/elfeed-goodies/master/data/screenshot.png" >}}

El segundo es el complemento mas a destacar; configurar y gestionar
los feeds con un **archivo.org**. Como se puede ver con la configuración
por defecto en elfeed, al incluir tus feeds dentro del `init` es mas
caótica su correcta administración, es mejor **mantener tus archivos
emacs separados de tus archivos personales**.


## Bonus: Agregar RSS de youtube {#bonus-agregar-rss-de-youtube}

Una de las grandes ventajas de lectores como liferea es que al
ingresar una dirección, en youtube es mas evidente, busca la
direción RSS automáticamente. Con nuestros archivos, al ser texto
plano no podemos hacer lo mismo, si ponemos la dirección así nomas:
`https://www.youtube.com/usuario/alguien` nos dará error ya que no
contiene formato RSS alguno. Es **necesario usar estas url** de acuerdo a
estas situaciones:

-   Para canales con la dirección **_user_**:

<https://www.youtube.com/user/escuelacuadros>

usar:

<https://www.youtube.com/feeds/videos.xml?user=escuelacuadros>

-   Para canales con la dirección **_channel_**:

<https://www.youtube.com/channel/UCkUQTzZLHojhlPtSvIOIf8A>

usar:

<https://www.youtube.com/feeds/videos.xml?channel%5Fid=UCkUQTzZLHojhlPtSvIOIf8A>

---

De esta manera y usando control de versiones, como git, la tarea de
agregar/retirar feeds se nos facilitará, perdiendo el menor tiempo
posible.

**¡SIGUE TUS FUENTES!** 📢 📢 📢
