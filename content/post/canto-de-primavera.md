+++
title = "Canto de primavera"
author = ["Mickie"]
date = 2019-03-30T16:53:00-06:00
lastmod = 2019-04-02T11:42:26-06:00
tags = ["literatura", "cultura"]
draft = false
+++

{{< figure src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Glifo%5Fprehisp%C3%A1nico%5Fde%5FNezahualc%C3%B3yotl.svg/240px-Glifo%5Fprehisp%C3%A1nico%5Fde%5FNezahualc%C3%B3yotl.svg.png" >}}

En la casa de las pinturas<br />
comienza a cantar,<br />
ensaya el canto,<br />
derrama flores,<br />
alegra el canto.<br />
<br />
Resuena el canto,<br />
los cascabeles se hacen oír,<br />
a ellos responden<br />
nuestras sonajas floridas.<br />
Derrama flores,<br />
alegra el canto.<br />
<br />
Sobre las flores canta<br />
el hermoso faisán,<br />
su canto despliega<br />
en el interior de las aguas.<br />
A él responden<br />
varios pájaros rojos,<br />
el hermoso pájaro rojo<br />
bellamente canta.<br />
<br />
Libro de pinturas es tu corazón,<br />
has venido a cantar,<br />
haces resonar tus tambores,<br />
tú eres el cantor.<br />
En el interior de la casa de la primavera,<br />
alegras a las gentes.<br />
<br />
Tú solo repartes<br />
flores que embriagan,<br />
flores preciosas.<br />
Tú eres el cantor<br />
En el interior de la casa de la primavera,<br />
alegras a las gentes.<br />
<br />
--- Nezahualcóyotl<br />

---

Ms. _Romances de los señores de la Nueva España_, Colección Latinoamericana, Universidad de Texas, fols. 38v. - 39r.
