---
title: Acerca de
comments: false
---
## yo
Mi nombre es Miguel, aka mickie, licenciado en ingeniería ambiental.
Aficionado a múltiples campos del quehacer humano. Lector noctámbulo y hacedor de ideas.
De un lado pá otro buscando el punto de fuga.

- Bienvenido al blog :)

### este blog
Este pequeño espacio es una especie de bloc de notas a grandes rasgos.

Dejar apuntes, bitácora, y un largo etc. en un mismo sitio a donde regresar y tener las cosas organizadas.

De temática libre, donde se publicará un poco de todo y algo de nada; lo que vaya rescatándose del tintero, o surja
del calor a flor de piel del momento.

### contacto

**diaspora*:** [mike180@diasp.org](https://diasp.org/people/910b7d507f440132241100259069449e)

**mastodon:** [@alejandro@mastodon.host](https://mastodon.host/@alejandro)

**openstreetmap:** [Mike95](https://wiki.openstreetmap.org/wiki/User:Mike95)

**Correo:** millet [at] tuta.io

#### comentarios
Solo ocupas _loguearte_ con una cuenta de **github**.

Los comentarios se publican como **_issues_** en este [repositorio](https://github.com/mike-alex/comments).
